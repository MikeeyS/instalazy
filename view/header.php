<!DOCTYPE html>
<html lang="en">
<head>
<title>InstaLazy</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://fonts.googleapis.com/css?family=Nunito&display=swap" rel="stylesheet">
 <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>

<header>
<nav class="navbar is-light center" role="navigation" aria-label="main navigation">
  <div class="navbar-brand">
    <a class="navbar-item" href="index.php">
      <img src="assets/images/logo.png">
    </a>

    <a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
    </a>
  </div>

  <div id="navbarBasicExample" class="navbar-menu">
    <div class="navbar-start">
      <a class="navbar-item">
        Home
      </a>

      <a class="navbar-item">
        Documentation
      </a>

      <div class="navbar-item has-dropdown is-hoverable">
        <a class="navbar-link">
          More
        </a>

        <div class="navbar-dropdown">
          <a class="navbar-item">
            About
          </a>
          <a class="navbar-item">
            Jobs
          </a>
          <a class="navbar-item">
            Contact
          </a>
          <hr class="navbar-divider">
          <a class="navbar-item">
            Report an issue
          </a>
        </div>
      </div>
    </div>

    <div class="navbar-end">
      <div class="navbar-item">
        <div class="buttons">
          <a href="generate.php" class="button is-primary">
            <strong>Generate post</strong>
          </a>

          <?php if (!isset($_SESSION['username'])) : ?>
          	<a class="button is-light">
            Log in
          </a>
          <?php endif; ?>
        </div>
      </div>
       <?php if (isset($_SESSION['username'])) : ?>
       <div class="navbar-item has-dropdown is-hoverable">
        <a class="navbar-link">
         <?php echo $_SESSION['username']; ?>
        </a>

        <div class="navbar-dropdown">
          <a class="navbar-item">
            My Profile
          </a>
          <a class="navbar-item">
            My posts
          </a>
          <a href="addhash.php" class="navbar-item">
            My Hashtags
          </a>
          <hr class="navbar-divider">
          <a href ="logout.php" class="navbar-item">
            Log out
          </a>
        </div>
      </div>
      <?php endif; ?>
    </div>
  </div>
</nav>
</header>