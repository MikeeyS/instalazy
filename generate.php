    <?php 
    include('config/database.php');
    include('view/header.php'); 

    $userID = array();
    $errors = array(); 
    $v = '';
    if (isset($_SESSION['username'])) : {

        $usernameSession = $_SESSION['username'];
        $checkuser_id = "SELECT * FROM users WHERE username= '$usernameSession'";
        $result = mysqli_query($db, $checkuser_id);
        $user = mysqli_fetch_assoc($result);
        $userID = $user['id'];

        $comments_query = "SELECT comments FROM comments WHERE id = '$userID'";
        $comments_result = mysqli_query($db, $comments_query);
        $comments = mysqli_fetch_all($comments_result, MYSQLI_ASSOC);

        $k = array_rand($comments);
        $v = $comments[$k];

     } endif;

     //echo $v['comments'];

    ?>

    <section class="section">
    <div class="container">
    	<div class="columns is-centered">
      <div class="column is-half">
      	 <form method="POST" action="generate.php">
        <?php include('config/errors.php'); ?>

    <div class="field">
      <label class="label">Question</label>
      <div class="control">
        <input class="input" name="g-question" type="text" placeholder="Text input">
      </div>
    </div>

    <div class="field">
      <label class="label">Credit author</label>
      <div class="control">
        <input class="input" type="text" name="g-credit" placeholder="Text input">
      </div>
    </div>


    <div class="field is-grouped">
      <div class="control">
        <button type="submit" class="button is-link" name="ggenerate">Submit</button>
      </div>
    </div>
     </form>
    </div>

      </div>

      <div class="preview">
               <?php 
               if (isset($_POST['ggenerate'])) : {
                       echo $_POST['g-question'] . '</br>';
                       echo $_POST['g-credit'] . '</br>';
                       echo $v['comments'];  
                 } endif;
               ?>
      </div>
      
    </div>
    </section>


     <?php 
    include('view/footer.php');
     ?>